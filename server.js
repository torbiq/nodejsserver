'use strict';

const express = require('express');
const http =  require('http');
const socketio = require('socket.io');
const mongodb = require('mongodb');
const url = "mongodb://localhost:12345/mydb";

// Platform defined enum on client
const PLATFORM_ANDROID = 0;
const PLATFORM_IOS = 1;
const PLATFORM_STANDALONE = 2;
//----------------------------

// API calls from client
const APIconnection = 'connection';
const APIlogin = 'login';
const APIcarSerialized = 'car_serialized';
const APIgameRequest = 'game_request';
const APIgameFinished = 'game_finished';
const APIdisconnect = 'disconnect';
//----------------------------

// API calls from server
const APIgameResponse = 'game_response';
//----------------------------

class AccountInfo {
    constructor(deviceInfo, nick, car) {
        this.deviceInfo = deviceInfo;
        this.nick = nick;
        this.car = car;
    }
}
class GameResponse {
    constructor(car, nick) {
        this.car = car;
        this.nick = nick;
    }
}
class DeviceInfo {
    constructor(platform, UniqueID, Name, Model, Type, gpuID, gpuName, gpuType, os, cpu) {
        this.platform = platform;
        this.UniqueID = UniqueID;
        this.Name = Name;
        this.Model = Model;
        this.Type = Type;
        this.gpuID = gpuID;
        this.gpuName = gpuName;
        this.gpuType = gpuType;
        this.os = os;
        this.cpu = cpu;
    }
}
class GameRequest {
    constructor(isRandom, opponentID) {
        this.isRandom = isRandom;
        this.opponentID = opponentID;
    }
}

class Server {
    constructor() {
        this.port = 3000;
        this.host = '0.0.0.0';
        this.app = express();
        this.server = this.app.listen(this.port);
        this.socket = socketio(this.server);
        this.clientsConnected = [];
        this.mongoClient = mongodb.MongoClient;
        this.db = null;
        
    }
    exitHandler() {
        this.db.close((res)=>{
            
        });
    }
    appExecute() {
        this.mongoClient.connect(url, (err, db)=>{
            if (err) throw err;
            this.db = db;
            console.log(`this db name: ${this.db.databaseName} collections: ${this.db.collections.length}`);
            this.db.createCollection('players', function(err, res) {
                if (err) throw err;
                console.log('Players collection initialized...');
                console.log('Current players in db: ');

                // db.collection('players').deleteMany({}, (err, res) => {
                db.collection('players').find().toArray(function(errorFind, docs) {
                    if (errorFind) throw errorFind;
                    console.log("-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=");
                    console.log(`Currently ${docs.length} players in db.`);
                    for (let i = 0; i < docs.length; i++) {
                        console.log(` ${docs[i].device_id}`);
                        console.log(`Car: ${docs[i].car_serialized}`);
                    }
                    console.log("-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=");
                });
            // });
            });
        });
        this.server.addListener('listening', () => {
            console.log(`Listening on ${this.host}:${this.port}`);
            this.socket.on(APIconnection, (client) => {

                this.clientsConnected.push(client);
                console.log('Connected: %s sockets connected', this.clientsConnected.length);
                
                client.on(APIlogin, (deviceInfo)=>{
                    console.log('Try login user...');
                    let devInfo = undefined;
                    try {
                        devInfo = new DeviceInfo(deviceInfo.platform, 
                            deviceInfo.UniqueID, 
                            deviceInfo.Name, 
                            deviceInfo.Model, 
                            deviceInfo.Type, 
                            deviceInfo.gpuID, 
                            deviceInfo.gpuName, 
                            deviceInfo.gpuType, 
                            deviceInfo.os,
                            deviceInfo.cpu);
                    }
                    catch (ex) {
                        console.log('Login fail.');
                        console.log('Uncorrect device info received.');
                        console.log(`Exception: ${ex}`);
                        // throw ex;
                    }
                    finally {
                        if (devInfo == undefined) {
                            return;
                        }

                        client.device_id = devInfo.UniqueID;
                        client.device_info = devInfo;

                        console.log('Login done...');
                        
                        let query = { 
                            device_id: devInfo.UniqueID 
                        }
                        let updatedInfo = {
                            $set: {
                                device_id: devInfo.UniqueID,
                                device_info: devInfo
                            } 
                        }
                        let options = {
                            upsert: true,
                            multi: false
                        }
                        this.db.collection('players').updateOne(query, updatedInfo, options, (err, res) => {
                            // console.log(err);
                            if (err) throw err;
                            // console.log(res.matchedCount + ' document(s) found' + res.result.nModified + res.result.nUpserted + ' document(s) upserted');
                            if (res.result.nUpserted > 0) {
                                console.log('Account created on device_id: ' + client.device_id);
                            }
                            if (res.result.nMatched > 0) {
                                console.log('Logged in by device_id: ' + client.device_id);
                            }
                        });
                            console.log('Current players in db: ');
                            this.db.collection('players').find().toArray(function(errorFind, docs) {
                                if (errorFind) throw errorFind;
                                console.log("-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=");
                                console.log(`Currently ${docs.length} players in db.`);
                                for (let i = 0; i < docs.length; i++) {
                                    console.log(` ${docs[i].device_id}`);
                                    console.log(`Car: ${docs[i].car_serialized}`);
                                }
                                console.log("-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=");
                            });
                        }
                });

                client.on(APIcarSerialized, (car)=>{
                    console.log('Car serialized save request event from ' + client.device_id);
                    let query = { 
                        device_id: client.device_id 
                    }
                    let updatedInfo = {
                        $set: {
                            car_serialized: car,
                        } 
                    }
                    let options = {
                        upsert: false,
                        multi: true,
                    }
                    this.db.collection('players').update(query, updatedInfo, options, (err, res)=>{
                        if (err) throw err;
                        console.log(res.matchedCount + ' document(s) found ' + res.result.nModified + ' document(s) modified');
                    });
                });

                client.on(APIgameRequest, (game_request) => {
                    console.log('Game request event from ' + client.device_id);
                    let query = { 
                        device_id: { $ne : client.device_id },
                        car_serialized: { $ne : null }
                    }
                    this.db.collection('players').findOne(query).then((documentFound)=>{
                        if (documentFound) {
                            console.log(`Both of these shit are equal: ${documentFound.device_id == client.device_id}`);
                            console.log('Found: car_serialized: ' + documentFound.car_serialized);
                            console.log('Found: device_id: ' + documentFound.device_id);
                            // let gresponse = new GameResponse(JSON.stringify(documentFound.car_serialized), JSON.stringify(documentFound.device_id));
                            // let asJSON = JSON.stringify(gresponse);
                            // console.log(`Response code '${APIgameResponse}' : ${asJSON}`);
                            // this.socket.to(client.id).emit(APIgameResponse, asJSON);
                            this.socket.to(client.id).emit(APIgameResponse, {
                                car: documentFound.car_serialized,
                                nick: documentFound.device_id
                            });
                        }
                        else {
                            console.log('No other player cars available on server...');
                        }
                    });
                });

                client.on(APIdisconnect, (data) => {
                    this.clientsConnected.splice(this.clientsConnected.indexOf(client), 1);
                    console.log('Disconnected: %s sockets connected', this.clientsConnected.length);
                });
            });
        });
    }
}

const server = new Server();
process.on('exit', server.exitHandler);
server.appExecute();