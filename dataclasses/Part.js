'use strict';

const Bonus = require('./Bonus');
const Chartics = require('./Chartics');

const PartMainType = {
    Body: 0,
    Wheel: 1,
    Weapon: 2,
    Item: 3,
};

const BodySubType = {
    Classic: 0,
};
const WheelSubType = {
    Bigfoot: 0,
};
const WeaponSubType = {
    Rocket: 0,
    Blade: 1,
};
const ItemSubType = {
    Reverser: 0,
};

class Part {
    constructor(mainType, subType, chartics, bonus) {
        this.mainType = mainType;
        this.subType = subType;
        this.chartics = chartics;
        this.bonus = bonus;
    }
}

exports.PartMainType = PartMainType;
exports.BodySubType = BodySubType;
exports.WheelSubType = WheelSubType;
exports.WeaponSubType = WeaponSubType;
exports.ItemSubType = ItemSubType;
exports.Part = Part;