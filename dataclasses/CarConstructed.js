'use strict';

const Body = require('./parts/Body').Body;

class CarConstructed {
    constructor(body) {
        this.body = body;
    }
    attach(body) {
        this.body = body;
    }
    detach() {
        let body = this.body;
        this.body = null;
        return body;
    }
}

exports.CarConstructed = CarConstructed;