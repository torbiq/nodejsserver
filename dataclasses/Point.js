'use strict';
const Vector3 = require('./Vector3');
const Part = require('./Part');

const PointType = {
    WHEEL: 0,
    WEAPON: 1,
    ITEM: 2,
};

class Point {
    constructor(pos, part, type) {
        this.pos = pos;
        this.part = part;
        this.type = type;
    }

    attach(part) {
        this.part = part;
    }
    
    detach() {
        let part = this.part;
        this.part = null;
        return part;
    }
}

exports.PointType = PointType;
exports.Point = Point;