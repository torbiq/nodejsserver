'use strict';

const PartMainType = require('../Part').PartMainType;
const BodySubType = require('../Part').BodySubType;
const Part = require('../Part').Part;

class Body extends Part {
    constructor(carConstructed, itemPoints, weaponPoints, wheelPoints, subType, chartics, bonus) {
        super(PartMainType.Body, BodySubType.Classic, chartics, bonus);
        this.carConstructed = carConstructed;
        this.itemPoints = itemPoints;
        this.weaponPoints = weaponPoints;
        this.wheelPoints = wheelPoints;
    }
}

exports.Body = Body;