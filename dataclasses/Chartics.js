'use strict';

const MAX_PROGRESS = 100;
const DEFAULT_LEVEL = 1;
const DEFAULT_PROGRESS = 0;
const Bonus = require('./Bonus').Bonus;

class Chartics {
    constructor(hp, damage, energy, level, progress) {
        this.hp = hp;
        this.damage = damage;
        this.energy = energy;
        this.level = level;
        this.progress = progress;
        this.bonusesAffecting = [];
    };
    addProgress(progress) {
        this.progress += progress;
        if (this.progress >= MAX_PROGRESS) {
            this.level += this.progress / MAX_PROGRESS;
            this.progress %= MAX_PROGRESS;
            // ADD UPGRADE HERE!!!
        }
    };
    addBonus(bonus) {
        this.bonusesAffecting.push(bonus);
    };
    removeBonus(bonus) {
        this.bonusesAffecting.splice(indexOf(bonus), 1);
    };
};

exports.Chartics = Chartics;