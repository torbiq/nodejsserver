'use strict';
const Part = require('./Part').Part;

const BonusValueType = {
    HP: 0,
    DAMAGE: 1
};
const BonusAffectingType = {
    ON_ATTACHED: 0,
    IF_ATTACHED: 1
};

class Bonus {
    constructor(valueTypeAffected, affectingType, partAttahcedTo, multiplier, subTypeEffector) {
        this.valueTypeAffected = valueTypeAffected;
        this.affectingType = affectingType;
        this.partAttahcedTo = partAttahcedTo;
        this.multiplier = multiplier;
        this.subTypeEffector = subTypeEffector;
        this.value = 0;
    };
    activate(parts) {
        for (let i = 0; i < parts.length; i++) {
            var part = parts[i];
            if (part.subType == this.subTypeEffector) {
                if (this.affectingType == BonusAffectingType.ON_ATTACHED) {
                    if (this.valueTypeAffected == BonusValueType.HP) {
                        this.value = this.multiplier * part.hp;
                    }
                    else {
                        this.value = this.multiplier * part.damage;
                    }
                }
                else {
                    if (this.valueTypeAffected == BonusValueType.HP) {
                        this.value = this.multiplier * this.partAttahcedTo.hp;
                    }
                    else {
                        this.value = this.multiplier * this.partAttahcedTo.damage;
                    }
                }
                break;
            }
        }
    }
    deactivate() {
        value = 0;
    }
};

exports.BonusValueType = BonusValueType;
exports.BonusAffectingType = BonusAffectingType;
exports.Bonus = Bonus;